package main

import (
	"fmt"
	"reflect"
	"testing"
)

func Test(t *testing.T) {
	//noinspection SpellCheckingInspection
	tests := []struct {
		text           string
		phrase         string
		expectedResult []int
	}{
		{"abcabaabcabac", "abaa", []int{3}},
		{"carpet needs cleaning", "lean", []int{14}},
		{"carpet needs cleaning please", "ea", []int{15, 24}},
		{"My not so clean carpet needs cleaning please", "clean", []int{10, 29}},
	}

	for _, test := range tests {
		t.Run("Table Test", func(t *testing.T) {
			result := match(test.text, test.phrase)
			fmt.Printf("Input TEXT:   %v\n", test.text)
			fmt.Printf("Input PHRASE: %v\n", test.phrase)
			fmt.Printf("Expected output: %v\n", test.expectedResult)

			if !reflect.DeepEqual(result, test.expectedResult) {
				t.Errorf("Expected %v but got %v instead!\n", test.expectedResult, result)
			}
		})
	}

}
