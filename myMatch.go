package main

import (
	"fmt"
)

type transitionInput struct {
	state int
	input string // char
}

type fsm struct {
	//states          []int
	startState      int
	acceptState     int
	transitionTable map[transitionInput]int
}

func printTextAndPattern(text string, pattern string, patternShift int) {
	fmt.Printf("%v\n", text)
	for ; patternShift > 0; patternShift-- {
		print("-")
	}
	fmt.Printf("%v\n\n", pattern)
}

func naiveMatching(text string, pattern string) (suffixes []int) {

	// Loop through the text, each iteration shifting the phrase by one
	// START AT:	0
	// FINISH AT:	Beginning of the last possible location of the phrase
	// (No early exits)
	for i := 0; i <= (len(text) - len(pattern)); i++ {

		//printTextAndPattern(text, pattern, i)
		//time.Sleep(500 * time.Millisecond)
		mismatch := false

		// Loop through the pattern
		// EXIT IF:		There is a mismatch
		// 	 OR IF:		All letters of the pattern match
		for j := 0; j < len(pattern) && !mismatch; j++ {

			if text[i+j] != pattern[j] {
				mismatch = true
			}
		}

		// If the loop exits
		// AND
		// there is still no mismatch
		// THEN
		// the pattern has been found in the text
		if !mismatch {
			//fmt.Printf("FOUND A MATCH AT i = %v\n\n", i)
			suffixes = append(suffixes, i)
		}

	}

	return suffixes
}

func generateTransitions(pattern string, alphabet []string) map[transitionInput]int {
	// IDEA: Use slices from length = 1 to max length and put these through naive matching
	// Select the maximum ?

	transitions := make(map[transitionInput]int)

	// Loop through each possible currently reached state
	for reachedState := 0; reachedState <= len(pattern); reachedState++ {

		// Loop through each possible input
		for _, c := range alphabet {

			// Look at suffixes starting at full string and reducing in length (knock off the front char)
			// Align the beginning of the suffix with beginning of the text
			// If there is a match, add a transition and break the loop
			for start := 0; start <= len(pattern) && start <= reachedState; start++ {

				newString := pattern[start:reachedState] + c

				if suffixMatchesText(pattern, newString) {
					input := transitionInput{state: reachedState, input: c}
					transitions[input] = len(newString)
					break

				}
			}

		}
	}

	return transitions
}

func suffixMatchesText(pattern, newString string) bool {
	shifts := naiveMatching(pattern, newString)
	return len(shifts) != 0 && shifts[0] == 0
}

func generateAlphabet(pattern string) (alphabet []string) {
	for i := range pattern {
		alphabet = append(alphabet, pattern[i:i+1])
	}
	//fmt.Printf("%v\n", alphabet)
	return
}

func generateFsm(pattern string) fsm {
	fsm := fsm{
		startState:      0,
		acceptState:     len(pattern),
		transitionTable: generateTransitions(pattern, generateAlphabet(pattern)),
	}

	return fsm
}

func doTransition(f fsm, input transitionInput) int {
	state, present := f.transitionTable[input]

	if present {
		return state
	} else {
		return 0
	}
}

func fsmMatching(text string, pattern string) (suffixes []int) {

	f := generateFsm(pattern)

	state := f.startState

	for i := range text {
		state = doTransition(f, transitionInput{state, text[i : i+1]})
		fmt.Printf("STATE: %v\n", state)
		if state == f.acceptState {
			fmt.Printf("HERE!\n")
			suffixes = append(suffixes, i+1-len(pattern))
		}
	}

	return
}

func kmpMatching(text string, pattern string) (suffixes []int) {
	suffixes = []int{-1}
	return suffixes
}

// Returns all valid shifts s.t. the pattern occurs in the text with the shift s
func match(text string, pattern string) []int {
	//return naiveMatching(text, pattern)
	return fsmMatching(text, pattern)
}

func main() {
	//generateTransitions("this is this a test", []string{"t", "h", "i", "s", "a", "e"})
	//generateTransitions("hell hello", []string{"h", "e", "l", "o"})
	//generateTransitions("bbaabba", []string{"a", "b"})
	generateFsm("bbaabba")
	//generateTransitions("abababababa", []string{"a", "b"})
}
